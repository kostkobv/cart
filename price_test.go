package cart_test

import (
	"testing"

	"gitlab.com/kostkobv/cart"
)

func TestPrice_String(t *testing.T) {
	tt := []struct {
		name   string
		result string
		price  cart.Price
	}{
		{
			name:   "price smaller than 1 cent",
			price:  cart.Price{Currency: cart.CurrencyAUD, Amount: 1},
			result: "$0.00",
		},
		{
			name:   "always shows cents",
			price:  cart.Price{Currency: cart.CurrencyAUD, Amount: cart.CurrMultiplier},
			result: "$1.00",
		},
		{
			name:   "always shows double precision cents",
			price:  cart.Price{Currency: cart.CurrencyAUD, Amount: 3 * cart.CurrMultiplier / 10},
			result: "$0.30",
		},
		{
			name:   "shows commas for thousands",
			price:  cart.Price{Currency: cart.CurrencyAUD, Amount: 312312321},
			result: "$31,231.23",
		},
		{
			name:   "shows commas for millions",
			price:  cart.Price{Currency: cart.CurrencyAUD, Amount: 313212312321},
			result: "$31,321,231.23",
		},
		{
			name:   "rounds to full if doesn't fit cents",
			price:  cart.Price{Currency: cart.CurrencyAUD, Amount: 9999},
			result: "$1.00",
		},
		{
			name:   "picks up default format if no currency provided or unknown",
			price:  cart.Price{Amount: 313212312321},
			result: "31,321,231.23",
		},
	}

	for _, test := range tt {
		t.Run(test.name, func(t *testing.T) {
			result := test.price.String()
			if test.result != result {
				t.Errorf("Not equal:\nExpected: %s\nActual: %s\n\n", test.result, result)
				t.Fail()
			}
		})
	}
}

func TestPrice_Validate(t *testing.T) {
	tt := []struct {
		name   string
		errMsg string
		p      *cart.Price
		err    bool
	}{
		{
			name:   "invalid currency returns error",
			p:      &cart.Price{},
			err:    true,
			errMsg: "invalid currency ",
		},
		{
			name:   "invalid amount returns error",
			p:      &cart.Price{Currency: cart.CurrencyAUD},
			err:    true,
			errMsg: "price cannot be equal or smaller than zero",
		},
		{
			name: "valid",
			err:  false,
			p:    &cart.Price{Currency: cart.CurrencyAUD, Amount: 1},
		},
	}

	for _, test := range tt {
		t.Run(test.name, func(t *testing.T) {
			err := test.p.Validate()
			errNotNil := (err != nil)
			if errNotNil != test.err {
				t.Errorf("unexpected error value: %s", err)
				return
			}

			if !errNotNil {
				return
			}

			if err.Error() != test.errMsg {
				t.Errorf("Expected: %s; Actual: %s;", test.errMsg, err.Error())
				return
			}
		})
	}
}
