package cart

import "testing"

func TestNewProduct(t *testing.T) {
	tt := []struct {
		name  string
		pName string
		sku   string
		price Price
		check func(*testing.T, *Product, error)
	}{
		{
			name:  "returns error if no sku provided",
			price: Price{Amount: 1, Currency: CurrencyAUD},
			check: func(t *testing.T, _ *Product, err error) {
				if err == nil {
					t.Errorf("expected error")
					return
				}

				e := "sku is required"
				a := err.Error()
				if e != a {
					t.Errorf("Expected: %s; Actual: %s;", e, a)
					return
				}
			},
		},
		{
			name:  "returns error if no product name provided",
			sku:   "123",
			price: Price{Amount: 1, Currency: CurrencyAUD},
			check: func(t *testing.T, _ *Product, err error) {
				if err == nil {
					t.Errorf("expected error")
					return
				}

				e := "name is required"
				a := err.Error()
				if e != a {
					t.Errorf("Expected: %s; Actual: %s;", e, a)
					return
				}
			},
		},
		{
			name:  "returns error if provided price is not valid",
			sku:   "123",
			pName: "name",
			price: Price{Currency: CurrencyAUD},
			check: func(t *testing.T, _ *Product, err error) {
				if err == nil {
					t.Errorf("expected error")
					return
				}

				e := "price cannot be equal or smaller than zero"
				a := err.Error()
				if e != a {
					t.Errorf("Expected: %s; Actual: %s;", e, a)
					return
				}
			},
		},
		{
			name:  "returns product is all valid",
			sku:   "123",
			pName: "name",
			price: Price{Amount: 1, Currency: CurrencyAUD},
			check: func(t *testing.T, _ *Product, err error) {
				if err != nil {
					t.Errorf("Expected: nil; Actual: %s;", err)
					return
				}
			},
		},
	}

	for _, test := range tt {
		t.Run(test.name, func(t *testing.T) {
			p, err := NewProduct(test.sku, test.pName, test.price)
			test.check(t, p, err)
		})
	}
}
