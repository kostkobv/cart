package cart

import (
	"errors"
	"fmt"
	"log"
)

// Operation that should be taken to properly
// apply the discount.
type Operation string

// Possible operations.
const (
	OperationEq     Operation = "="
	OperationMoreEq Operation = ">="
)

// ActionType describes what kind of Discount is provided.
type ActionType string

// Possible ActionType's.
const (
	ActionTypeFreeProduct ActionType = "free_product"

	// Action Value should be set the percents of the discount.
	ActionTypePercentageDiscount ActionType = "percentage_discount"
)

// Action of Discount.
type Action struct {
	Type ActionType

	// Value of the discount.
	// Depends on a type.
	Value int64

	// Amount of items to which
	// discount should be applied.
	// If Amount is -1 - discount applies to
	// all of the products.
	Amount int64

	// SKU to which the discount should be applied.
	SKU string
}

// Applicable rule for Discount.
type Applicable struct {
	SKU       string
	Amount    int64
	Operation Operation
}

// Discount representation that can be applied to the products.
type Discount struct {
	ID   string
	Name string

	// List of SKU's to which
	// discount applies.
	AppliesTo map[string]Applicable

	// What exactly should be applied.
	Action Action
}

// Validate the Discount.
func (d *Discount) Validate() error {
	switch {
	case d.ID == "":
		return errors.New("id is not set")
	case len(d.AppliesTo) == 0:
		return errors.New("discount doesn't apply to anything")
	case d.Action.SKU == "":
		return errors.New("action sku is required")
	case d.Action.Amount == 0:
		return errors.New("action amount or all products should be provided")
	}

	return nil
}

// Apply the discount to the provided items.
// Returns price amount sum of the applicable items after discount, items to which discount do not apply
// and error if any.
func (d *Discount) Apply(items []LineItem, curr Currency) (int64, []LineItem, error) {
	if len(items) == 0 {
		return 0, nil, ErrNoDiscounts
	}

	// select the correct result function
	// depending on the action type.
	var result resultFunc
	switch d.Action.Type {
	case ActionTypeFreeProduct:
		result = d.resultFreeProduct
	case ActionTypePercentageDiscount:
		result = d.resultPercentage
	default:
		return 0, nil, fmt.Errorf("unknown action type for discount: %s", d.Action.Type)
	}

	// check what is the amount of products
	// to which discount is applicable and separate them.
	appl, notAppl, err := d.separateForApply(items)
	if err != nil {
		return 0, nil, err
	}

	var totalPrice int64

	// find product to which discount should be applied and
	// calculate the total price for the rest of items meanwhile.
	var appld bool
	for _, it := range appl {
		if it.SKU != d.Action.SKU {
			totalPrice += (it.Price.Amount * it.Amount)
			continue
		}
		if appld {
			continue
		}

		p, err := d.applyResult(it, result)
		if err != nil {
			return 0, nil, err
		}

		totalPrice += p
		appld = true
	}

	return totalPrice, notAppl, nil
}

func (d *Discount) applyResult(item LineItem, result resultFunc) (totalPrice int64, err error) {
	if item.Amount == 0 {
		return 0, ErrNoDiscounts
	}

	// in case if discount doesn't apply
	// to all of the products of the type -
	// chop the rest and add to total price.
	if d.Action.Amount > 0 {
		a := item.Amount - d.Action.Amount
		if a < 0 {
			return 0, ErrNoDiscounts
		}

		item.Amount -= a
		totalPrice += a * item.Price.Amount
	}

	// discounted price per item.
	p, err := result(item.Price.Amount)
	if err != nil {
		return 0, err
	}

	totalPrice += p * item.Amount
	return
}

// separateForApply picks up the products that are fitting the
// rules of the discount and put them into refine (returned 1st) slice. Leftovers
// are sent to notApplicable slice (returned 2nd).
// If there is a need to separate one product by amount:
// working part would go to 1st slice, the rest - to the second.
func (d *Discount) separateForApply(items []LineItem) ([]LineItem, []LineItem, error) {
	if len(items) == 0 {
		return nil, nil, ErrNoDiscounts
	}

	var toRefine []LineItem
	var notAppl []LineItem

	// first check if amount of items is applicable
	// and filter the items with which discount would be working.
	for _, item := range items {
		at, ok := d.AppliesTo[item.SKU]
		if !ok {
			notAppl = append(notAppl, item)
			continue
		}

		// item is in the cart but amount might not be
		// valid for this discount to be applicable.
		da, err := chopAmount(item.Amount, at.Amount, at.Operation)
		if err != nil {
			return nil, nil, ErrNoDiscounts
		}

		// split the items for discount.
		// we need only amount that is provided in the
		// discount.
		ritem, nitem, err := splitItems(item, da)
		if err != nil {
			return nil, nil, ErrNoDiscounts
		}

		// there is item to refine - put it into the refine slice.
		if ritem != nil {
			toRefine = append(toRefine, *ritem)
		}

		// there are leftovers which are non-applicable -
		// put it into the non-applicable slice for the next
		// discount iteration.
		if nitem != nil {
			notAppl = append(notAppl, *nitem)
		}
	}

	if len(toRefine) != len(d.AppliesTo) {
		return nil, nil, ErrNoDiscounts
	}

	return toRefine, notAppl, nil
}

func splitItems(item LineItem, a int64) (*LineItem, *LineItem, error) {
	if a == 0 {
		return nil, &item, nil
	}

	// working amount is a difference
	// between the amount on the item and
	// the amount that needs to be splitted.
	// basically the rest of the amount.
	wa := item.Amount - a
	if wa < 0 {
		return nil, nil, errors.New("amount too small to split")
	}

	item.Amount = a
	out := &item

	var wout *LineItem
	if wa > 0 {
		witem := item
		witem.Amount = wa
		wout = &witem
	}

	return out, wout, nil
}

type resultFunc func(int64) (int64, error)

func (d *Discount) resultPercentage(p int64) (int64, error) {
	if p < 0 {
		return 0, ErrNoDiscounts
	}

	return p - (p * d.Action.Value / 100), nil
}

func (d *Discount) resultFreeProduct(p int64) (int64, error) {
	if p < 0 {
		return 0, ErrNoDiscounts
	}

	return 0, nil
}

func chopAmount(have int64, need int64, operator Operation) (int64, error) {
	switch operator {
	case OperationEq:
		if have >= need {
			return need, nil
		}
	case OperationMoreEq:
		if have >= need {
			return have, nil
		}
	}

	return 0, ErrNoDiscounts
}

// Apply discounts to provided items.
// Returns total price of the items after discount was applied.
func Apply(dscnts []Discount, curr Currency, items []LineItem) (Price, error) {
	switch {
	case len(items) == 0:
		return (Price{}), ErrIsEmpty
	case len(dscnts) == 0:
		return (Price{}), ErrNoDiscounts
	case curr == "":
		return (Price{}), ErrUnknownCurrency
	}

	// iterate through discounts.
	var totalPrice int64
	for _, dscnt := range dscnts {
		// apply discount to the cart until it's not applicable
		// in case of multiple sets for which discount is fair
		for {
			// returns price of the items with the applied discount,
			// and also the rest of items for which discount was not applicable,
			// items should be chained into the next
			// discount since discounts could be applied to specific set of products
			// only once.
			p, i, err := dscnt.Apply(items, curr)
			if err != nil {
				if err != ErrNoDiscounts {
					log.Printf("Cart: unexpected error on applying discount (%s): %s", dscnt.ID, err)
				}
				break
			}

			items = i
			totalPrice += p
		}
	}

	// price for the items that left
	for _, item := range items {
		totalPrice += item.Price.Amount * item.Amount
	}

	return Price{Amount: totalPrice, Currency: curr}, nil
}
