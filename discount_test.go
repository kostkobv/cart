package cart

import (
	"errors"
	"fmt"
	"testing"
)

func TestDiscount_Validate(t *testing.T) {
	tt := []struct {
		name string
		d    *Discount
		e    string
		err  bool
	}{
		{
			name: "returns error if id is not set",
			d:    &Discount{},
			err:  true,
			e:    "id is not set",
		},
		{
			name: "returns error if discount doesn't apply to anything",
			d:    &Discount{ID: "1"},
			err:  true,
			e:    "discount doesn't apply to anything",
		},
		{
			name: "returns error if sku is not set",
			d:    &Discount{ID: "1", AppliesTo: map[string]Applicable{"1": {SKU: "1"}}},
			err:  true,
			e:    "action sku is required",
		},
		{
			name: "returns error if action value and all products is not set",
			d: &Discount{ID: "1", AppliesTo: map[string]Applicable{"1": {SKU: "1"}}, Action: struct {
				Type   ActionType
				Value  int64
				Amount int64
				SKU    string
			}{SKU: "1"}},
			err: true,
			e:   "action amount or all products should be provided",
		},
		{
			name: "passes if only value is set",
			d: &Discount{ID: "1", AppliesTo: map[string]Applicable{"1": {SKU: "1"}}, Action: struct {
				Type   ActionType
				Value  int64
				Amount int64
				SKU    string
			}{SKU: "1", Amount: 1}},
			err: false,
		},
		{
			name: "passes if only all products is set",
			d: &Discount{ID: "1", AppliesTo: map[string]Applicable{"1": {SKU: "1"}}, Action: struct {
				Type   ActionType
				Value  int64
				Amount int64
				SKU    string
			}{SKU: "1", Amount: -1}},
			err: false,
		},
	}

	for _, test := range tt {
		t.Run(test.name, func(t *testing.T) {
			err := test.d.Validate()
			errNotNil := err != nil
			if errNotNil != test.err {
				t.Error("unexpected error value")
				t.FailNow()
			}

			if !errNotNil {
				return
			}

			a := err.Error()

			if test.e != a {
				t.Errorf("Expected: %s; Actual: %s;", test.e, a)
				t.FailNow()
			}
		})
	}
}

func TestDiscount_resultPercentage(t *testing.T) {
	tt := []struct {
		name   string
		item   LineItem
		curr   Currency
		d      Discount
		e      Price
		err    bool
		errMsg string
	}{
		{
			name: "set's the discount price raw of percentage value provided in the discount",
			curr: CurrencyAUD,
			item: LineItem{
				Product: Product{
					Price: Price{
						Amount:   100 * CurrMultiplier,
						Currency: CurrencyAUD,
					},
				},
			},
			d: Discount{Action: struct {
				Type   ActionType
				Value  int64
				Amount int64
				SKU    string
			}{Value: 1}},
			e: Price{Amount: 99 * CurrMultiplier, Currency: CurrencyAUD},
		},
	}

	for _, test := range tt {
		t.Run(test.name, func(t *testing.T) {
			i, err := test.d.resultPercentage(test.item.Price.Amount)
			errNotNil := err != nil
			if errNotNil != test.err {
				t.Error("unexpected error value")
				t.FailNow()
			}

			if errNotNil {
				a := err.Error()
				if a != test.errMsg {
					t.Errorf("Expected: %s; Actual: %s;", test.errMsg, a)
					t.FailNow()
				}

				return
			}

			if i != test.e.Amount {
				t.Errorf("Expected: %#v; Actual: %#v;", test.e.Amount, i)
			}
		})
	}
}

func TestDiscount_resultFreeProduct(t *testing.T) {
	tt := []struct {
		name   string
		item   LineItem
		curr   Currency
		e      Price
		err    bool
		errMsg string
	}{
		{
			name: "set's the discount price raw of 0 with provided currency",
			curr: CurrencyAUD,
			e:    Price{Amount: 0, Currency: CurrencyAUD},
		},
	}

	for _, test := range tt {
		t.Run(test.name, func(t *testing.T) {
			i, err := (&Discount{}).resultFreeProduct(test.item.Price.Amount)
			errNotNil := err != nil
			if errNotNil != test.err {
				t.Error("unexpected error value")
				t.FailNow()
			}

			if errNotNil {
				a := err.Error()
				if a != test.errMsg {
					t.Errorf("Expected: %s; Actual: %s;", test.errMsg, a)
					t.FailNow()
				}

				return
			}

			if i != test.e.Amount {
				t.Errorf("Expected: %#v; Actual: %#v;", test.e.Amount, i)
			}
		})
	}
}

func Test_chopAmount(t *testing.T) {
	tt := []struct {
		name     string
		have     int64
		need     int64
		operator Operation
		e        int64
		err      error
	}{
		{
			name:     "invalid operator returns error",
			operator: "",
			err:      ErrNoDiscounts,
		},
		{
			name:     "equal operator returns need if more than have",
			operator: OperationEq,
			have:     10,
			need:     10,
			e:        10,
		},
		{
			name:     "equal operator returns need if need eq have",
			operator: OperationEq,
			have:     10,
			need:     7,
			e:        7,
		},
		{
			name:     "equal operator returns error if have less than need",
			operator: OperationEq,
			have:     5,
			need:     7,
			err:      ErrNoDiscounts,
		},

		{
			name:     "moreeq operator returns error if less than need",
			operator: OperationMoreEq,
			have:     9,
			need:     10,
			err:      ErrNoDiscounts,
		},
		{
			name:     "moreeq operator returns have if need eq have",
			operator: OperationMoreEq,
			have:     10,
			need:     10,
			e:        10,
		},
		{
			name:     "moreeq operator returns have if have more than need",
			operator: OperationMoreEq,
			have:     7,
			need:     5,
			e:        7,
		},
	}

	for _, test := range tt {
		t.Run(test.name, func(t *testing.T) {
			i, err := chopAmount(test.have, test.need, test.operator)
			if err != test.err {
				t.Errorf("unexpected error value")
				t.FailNow()
			}

			if i != test.e {
				t.Errorf("Expected: %d; Actual: %d;", test.e, i)
			}
		})
	}
}

func TestDiscount_separateForApply(t *testing.T) {
	compare := func(o []LineItem, oo []LineItem) error {
		if len(o) != len(oo) {
			return errors.New("different amount of items")
		}

		for i, item := range o {
			switch {
			case item.SKU != oo[i].SKU:
				return errors.New("SKU doesn't match")
			case item.Amount != oo[i].Amount:
				return fmt.Errorf("amount doesn't match: 1:%d, 2:%d", item.Amount, oo[i].Amount)
			}
		}

		return nil
	}

	tt := []struct {
		name  string
		d     Discount
		items []LineItem
		check func(t *testing.T, refined []LineItem, notAppl []LineItem, err error)
	}{
		{
			name: "no discounts if empty items",
			d:    Discount{},
			check: func(t *testing.T, refined []LineItem, notAppl []LineItem, err error) {
				if err != ErrNoDiscounts {
					t.Errorf("Expected: %s; Actual: %s;", ErrNoDiscounts, err)
				}
			},
		},
		{
			name: "applicable but not enough items",
			d: Discount{
				AppliesTo: map[string]Applicable{"1": {SKU: "1", Amount: 3, Operation: OperationEq}},
			},
			items: []LineItem{
				{Product: Product{SKU: "1"}, Amount: 2},
			},
			check: func(t *testing.T, refined []LineItem, notAppl []LineItem, err error) {
				if err != ErrNoDiscounts {
					t.Errorf("Expected: %s; Actual: %s;", ErrNoDiscounts, err)
				}
			},
		},
		{
			name: "no discount if doesn't have all the required items in the cart",
			d: Discount{
				AppliesTo: map[string]Applicable{
					"1": {SKU: "1", Amount: 1, Operation: OperationEq},
					"2": {SKU: "2", Amount: 1, Operation: OperationEq},
				},
			},
			items: []LineItem{
				{Product: Product{SKU: "1"}, Amount: 2},
			},
			check: func(t *testing.T, refined []LineItem, notAppl []LineItem, err error) {
				if err != ErrNoDiscounts {
					t.Errorf("Expected: %s; Actual: %s;", ErrNoDiscounts, err)
				}
			},
		},
		{
			name: "exact amount separated into refined",
			d: Discount{
				AppliesTo: map[string]Applicable{"1": {SKU: "1", Amount: 3, Operation: OperationEq}},
			},
			items: []LineItem{
				{Product: Product{SKU: "1"}, Amount: 3},
				{Product: Product{SKU: "2"}, Amount: 1},
			},
			check: func(t *testing.T, refined []LineItem, notAppl []LineItem, err error) {
				if err != nil {
					t.Errorf("unexpected error: %s", err)
					t.FailNow()
				}

				err = compare(refined, []LineItem{
					{Product: Product{SKU: "1"}, Amount: 3},
				})
				if err != nil {
					t.Errorf("Expected: %s; Actual: %s;", ErrNoDiscounts, err)
					t.FailNow()
				}

				err = compare(notAppl, []LineItem{
					{Product: Product{SKU: "2"}, Amount: 1},
				})
				if err != nil {
					t.Errorf("Expected: %s; Actual: %s;", ErrNoDiscounts, err)
					t.FailNow()
				}
			},
		},
		{
			name: "only required amount in refined, rest in not applicable",
			d: Discount{
				AppliesTo: map[string]Applicable{"1": {SKU: "1", Amount: 1, Operation: OperationEq}},
			},
			items: []LineItem{
				{Product: Product{SKU: "1"}, Amount: 3},
				{Product: Product{SKU: "2"}, Amount: 1},
			},
			check: func(t *testing.T, refined []LineItem, notAppl []LineItem, err error) {
				if err != nil {
					t.Errorf("unexpected error: %s", err)
					t.FailNow()
				}

				err = compare(refined, []LineItem{
					{Product: Product{SKU: "1"}, Amount: 1},
				})
				if err != nil {
					t.Errorf("Expected: %s; Actual: %s;", ErrNoDiscounts, err)
					t.FailNow()
				}

				err = compare(notAppl, []LineItem{
					{Product: Product{SKU: "1"}, Amount: 2},
					{Product: Product{SKU: "2"}, Amount: 1},
				})
				if err != nil {
					t.Errorf("Expected: %s; Actual: %s;", ErrNoDiscounts, err)
					t.FailNow()
				}
			},
		},
	}

	for _, test := range tt {
		t.Run(test.name, func(t *testing.T) {
			r, n, err := test.d.separateForApply(test.items)
			test.check(t, r, n, err)
		})
	}
}

func TestDiscount_Apply(t *testing.T) {
	tt := []struct {
		name  string
		d     *Discount
		items []LineItem
		curr  Currency
		check func(*testing.T, int64, []LineItem, error)
	}{
		{
			name: "returns error if empty items",
			d:    &Discount{},
			check: func(t *testing.T, _ int64, _ []LineItem, err error) {
				if err != ErrNoDiscounts {
					t.Errorf("Expected: %s; Actual: %s;", ErrNoDiscounts, err)
					return
				}
			},
		},
		{
			name: "returns error if can't separate applicable and not",
			d: &Discount{
				AppliesTo: map[string]Applicable{"1": {SKU: "1", Amount: 2}},
			},
			items: []LineItem{{Product: Product{SKU: "1"}, Amount: 1}},
			check: func(t *testing.T, _ int64, _ []LineItem, err error) {
				if err == nil || err.Error() != "unknown action type for discount: " {
					t.Errorf("Expected: %s; Actual: %s;", ErrNoDiscounts, err)
					return
				}
			},
		},
		{
			name: "able to handle percentage discount action",
			curr: CurrencyAUD,
			d: &Discount{
				ID:   "id",
				Name: "name",
				Action: Action{
					Type:   ActionTypePercentageDiscount,
					SKU:    "1",
					Amount: 1,
					Value:  10,
				},
				AppliesTo: map[string]Applicable{
					"1": Applicable{SKU: "1", Amount: 1, Operation: OperationEq},
				},
			},
			items: []LineItem{
				{
					Amount: 1,
					Product: Product{
						SKU: "1",
						Price: Price{
							Currency: CurrencyAUD,
							Amount:   10000,
						},
					},
				},
			},
			check: func(t *testing.T, _ int64, _ []LineItem, err error) {
				if err != nil {
					t.Errorf("Expected: nil; Actual: %s;", err)
				}
			},
		},
		{
			name: "returns error if invalid action type",
			curr: CurrencyAUD,
			d: &Discount{
				ID:   "id",
				Name: "name",
				Action: Action{
					Type:   "invalid",
					SKU:    "1",
					Amount: 1,
					Value:  10,
				},
				AppliesTo: map[string]Applicable{
					"1": Applicable{SKU: "1", Amount: 1, Operation: OperationEq},
				},
			},
			items: []LineItem{
				{
					Amount: 1,
					Product: Product{
						SKU: "1",
						Price: Price{
							Currency: CurrencyAUD,
							Amount:   10000,
						},
					},
				},
			},
			check: func(t *testing.T, _ int64, _ []LineItem, err error) {
				e := "unknown action type for discount: invalid"
				if err != nil && err.Error() != e {
					t.Errorf("Expected: %s; Actual: %s;", e, err.Error())
				}
			},
		},
		{
			name: "put items into non-applicable when too much for applicable",
			curr: CurrencyAUD,
			d: &Discount{
				ID:   "id",
				Name: "name",
				Action: Action{
					Type:   ActionTypeFreeProduct,
					SKU:    "1",
					Amount: 1,
				},
				AppliesTo: map[string]Applicable{
					"1": Applicable{SKU: "1", Amount: 1, Operation: OperationEq},
				},
			},
			items: []LineItem{
				{
					Amount: 10,
					Product: Product{
						SKU: "1",
						Price: Price{
							Currency: CurrencyAUD,
							Amount:   10000,
						},
					},
				},
			},
			check: func(t *testing.T, a int64, n []LineItem, err error) {
				if err != nil {
					t.Errorf("Expected: nil; Actual: %s;", err.Error())
					return
				}

				var e int64
				if a != e {
					t.Errorf("Expected: %d; Actual: %d;", e, a)
					return
				}

				nApp := n[0]
				switch {
				case nApp.Amount != 9:
					t.Errorf("Expected: %d; Actual: %d;", 1, nApp.Amount)
					return
				case nApp.SKU != "1":
					t.Errorf("Expected: %s; Actual: %s;", "1", nApp.SKU)
					return
				}
			},
		},
		{
			name: "splits applicable items amount when too much",
			curr: CurrencyAUD,
			d: &Discount{
				ID:   "id",
				Name: "name",
				Action: Action{
					Type:   ActionTypeFreeProduct,
					SKU:    "1",
					Amount: 1,
				},
				AppliesTo: map[string]Applicable{
					"1": Applicable{SKU: "1", Amount: 3, Operation: OperationEq},
				},
			},
			items: []LineItem{
				{
					Amount: 10,
					Product: Product{
						SKU: "1",
						Price: Price{
							Currency: CurrencyAUD,
							Amount:   10000,
						},
					},
				},
			},
			check: func(t *testing.T, a int64, n []LineItem, err error) {
				if err != nil {
					t.Errorf("Expected: nil; Actual: %s;", err.Error())
					return
				}

				var e int64 = 20000
				if a != e {
					t.Errorf("Expected: %d; Actual: %d;", e, a)
					return
				}

				if len(n) != 1 {
					t.Errorf("non-applicable line items is wrong")
					return
				}

				nApp := n[0]
				switch {
				case nApp.Amount != 7:
					t.Errorf("Expected: %d; Actual: %d;", 7, nApp.Amount)
					return
				case nApp.SKU != "1":
					t.Errorf("Expected: %s; Actual: %s;", "1", nApp.SKU)
					return
				}
			},
		},
		{
			name: "returns error if have not enough items in a cart to apply action",
			curr: CurrencyAUD,
			d: &Discount{
				ID:   "id",
				Name: "name",
				Action: Action{
					Type:   ActionTypeFreeProduct,
					SKU:    "1",
					Amount: 3,
				},
				AppliesTo: map[string]Applicable{
					"1": Applicable{SKU: "1", Amount: 1, Operation: OperationEq},
				},
			},
			items: []LineItem{
				{
					Amount: 1,
					Product: Product{
						SKU: "1",
						Price: Price{
							Currency: CurrencyAUD,
							Amount:   10000,
						},
					},
				},
			},
			check: func(t *testing.T, a int64, n []LineItem, err error) {
				if err != ErrNoDiscounts {
					t.Errorf("Expected: nil; Actual: %s;", err.Error())
					return
				}
			},
		},
	}

	for _, test := range tt {
		t.Run(test.name, func(t *testing.T) {
			tp, nonAppl, err := test.d.Apply(test.items, test.curr)
			test.check(t, tp, nonAppl, err)
		})
	}
}
