package main

import (
	"bufio"
	"errors"
	"fmt"
	"log"
	"os"
	"os/exec"
	"runtime"
	"strconv"

	"gitlab.com/kostkobv/cart"
)

func main() {
	d := discounts()
	inv := inventory()
	input := bufio.NewScanner(os.Stdin)
	crt := []cart.LineItem{}

	clrscr()

	for {
		printCart(crt, d)
		fmt.Println("==========================")
		printInv(inv)

		fmt.Printf("\nSelect item you want to add to your cart (1 - %d) and press 'Enter':\n", len(inv))

		var item cart.LineItem
		var iID int64
		for {
			input.Scan()

			var err error
			iID, err = parseID(input.Text(), 1, int64(len(inv)))
			if err != nil {
				fmt.Printf("Try again: %s\n", err)
				continue
			}
			iID--

			item = inv[iID]
			if item.Amount == 0 {
				fmt.Print("Try again: no items left in inventory\n")
				continue
			}

			break
		}

		fmt.Printf("\nSelect amount you want to add to your cart (1 - %d) and press 'Enter':\n", item.Amount)
		for {
			input.Scan()

			a, err := parseID(input.Text(), 1, item.Amount)
			if err != nil {
				fmt.Printf("Try again: %s\n", err)
				continue
			}

			item.Amount = a
			inv[iID].Amount -= a
			break
		}

		ci, err := itemBySKU(crt, item.SKU)
		if err != nil {
			crt = append(crt, item)
		} else {
			crt[ci].Amount += item.Amount
		}

		clrscr()
	}
}

func itemBySKU(items []cart.LineItem, sku string) (int64, error) {
	if sku == "" {
		return 0, errors.New("sku is required")
	}

	for i, item := range items {
		if item.SKU == sku {
			return int64(i), nil
		}
	}

	return 0, errors.New("not found")
}

func parseID(in string, min, max int64) (int64, error) {
	id, err := strconv.ParseInt(in, 10, 64)
	if err != nil {
		return 0, fmt.Errorf("invalid input: %s", err)
	}

	if id < min || id > max {
		return 0, fmt.Errorf("invalid input: should be between %d and %d", min, max)
	}

	return id, nil
}

func printCart(items []cart.LineItem, d []cart.Discount) {
	fmt.Println("Cart:")
	if len(items) == 0 {
		fmt.Print("Empty cart\n\n")
		return
	}

	for i, item := range items {
		fmt.Printf("%d). %s %s (qty: %d) \t - \t%s\n", i+1, item.SKU, item.Name, item.Amount, item.Price.String())
	}

	p, err := cart.Apply(d, cart.CurrencyAUD, items)
	if err != nil {
		log.Fatalf("Can't apply discounts: %s", err)
		return
	}

	fmt.Printf("Total price:\t\t\t\t - \t%s\n", p.String())
}

func printInv(items []cart.LineItem) {
	fmt.Println("Inventory:")
	if len(items) == 0 {
		fmt.Println("Empty")
		return
	}

	for i, item := range items {
		fmt.Printf("%d). %s %s (qty: %d) \t - \t %s \n", i+1, item.SKU, item.Name, item.Amount, item.Price.String())
	}
}

// clrscr - clears the screen in the terminal if supported.
func clrscr() {
	var c []string
	switch runtime.GOOS {
	case "linux", "darwin":
		c = []string{"clear"}
	case "windows":
		c = []string{"cmd", "/c", "cls"}
	default:
		return
	}

	cmd := exec.Command(c[0], c[1:]...)
	cmd.Stdout = os.Stdout
	cmd.Run()
}
