package main

import (
	"gitlab.com/kostkobv/cart"
)

func inventory() []cart.LineItem {
	return []cart.LineItem{
		{
			Product: cart.Product{
				SKU:   "120P90",
				Name:  "Google Home",
				Price: cart.Price{Amount: 499900, Currency: cart.CurrencyAUD},
			},
			Amount: 10,
		},
		{
			Product: cart.Product{
				SKU:   "43N23P",
				Name:  "MacBook Pro",
				Price: cart.Price{Amount: 53999900, Currency: cart.CurrencyAUD},
			},
			Amount: 5,
		},
		{
			Product: cart.Product{
				SKU:   "A304SD",
				Name:  "Alexa Speaker",
				Price: cart.Price{Amount: 1095000, Currency: cart.CurrencyAUD},
			},
			Amount: 10,
		},
		{
			Product: cart.Product{
				SKU:   "234234",
				Name:  "Raspberry Pi B",
				Price: cart.Price{Amount: 300000, Currency: cart.CurrencyAUD},
			},
			Amount: 2,
		},
	}
}
