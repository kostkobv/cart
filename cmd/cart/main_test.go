package main

import (
	"testing"

	"gitlab.com/kostkobv/cart"
)

func TestApp(t *testing.T) {
	tt := []struct {
		name  string
		e     string
		items []cart.LineItem
	}{
		{
			name: "Each sale of a MacBook Pro comes with a free Raspberry Pi B",
			items: []cart.LineItem{
				{
					Product: cart.Product{
						SKU:   "43N23P",
						Name:  "MacBook Pro",
						Price: cart.Price{Amount: 53999900, Currency: cart.CurrencyAUD},
					},
					Amount: 1,
				},
				{
					Product: cart.Product{
						SKU:   "234234",
						Name:  "Raspberry Pi B",
						Price: cart.Price{Amount: 300000, Currency: cart.CurrencyAUD},
					},
					Amount: 1,
				},
			},
			e: "$5,399.99",
		},
		{
			name: "Buy 3 Google Homes for the price of 2",
			items: []cart.LineItem{
				{
					Product: cart.Product{
						SKU:   "120P90",
						Name:  "Google Home",
						Price: cart.Price{Amount: 499900, Currency: cart.CurrencyAUD},
					},
					Amount: 3,
				},
			},
			e: "$99.98",
		},
		{
			name: "Buying more than 3 Alexa Speakers will have a 10% discount on all Alexa speakers",
			items: []cart.LineItem{
				{
					Product: cart.Product{
						SKU:   "A304SD",
						Name:  "Alexa Speaker",
						Price: cart.Price{Amount: 1095000, Currency: cart.CurrencyAUD},
					},
					Amount: 3,
				},
			},
			e: "$295.65",
		},
	}

	for _, test := range tt {
		t.Run(test.name, func(t *testing.T) {
			p, err := cart.Apply(discounts(), cart.CurrencyAUD, test.items)
			if err != nil {
				t.Errorf("Unexpected error: %s", err)
				return
			}

			a := p.String()
			if a != test.e {
				t.Errorf("Expected: %s; Actual: %s;", test.e, a)
				return
			}
		})
	}
}
