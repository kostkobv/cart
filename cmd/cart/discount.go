package main

import (
	"gitlab.com/kostkobv/cart"
)

func discounts() []cart.Discount {
	return []cart.Discount{
		{
			ID:   "macbook_with_rberry_pi",
			Name: "Each sale of a MacBook Pro comes with a free Raspberry Pi B",
			AppliesTo: map[string]cart.Applicable{
				"43N23P": {SKU: "43N23P", Amount: 1, Operation: cart.OperationEq},
				"234234": {SKU: "234234", Amount: 1, Operation: cart.OperationEq},
			},
			Action: cart.Action{
				Type:   cart.ActionTypeFreeProduct,
				Amount: 1,
				SKU:    "234234",
			},
		},
		{
			ID:   "3_google_homes_for_2",
			Name: "Buy 3 Google Homes for the price of 2",
			AppliesTo: map[string]cart.Applicable{
				"120P90": {SKU: "120P90", Amount: 3, Operation: cart.OperationEq},
			},
			Action: cart.Action{
				Type:   cart.ActionTypeFreeProduct,
				SKU:    "120P90",
				Amount: 1,
			},
		},
		{
			ID:   "alexa_10_percent",
			Name: "Buying more than 3 Alexa Speakers will have a 10% discount on all Alexa speakers",
			AppliesTo: map[string]cart.Applicable{
				"A304SD": {SKU: "A304SD", Amount: 3, Operation: cart.OperationMoreEq},
			},
			Action: cart.Action{
				Type:   cart.ActionTypePercentageDiscount,
				SKU:    "A304SD",
				Value:  10,
				Amount: -1,
			},
		},
	}
}
