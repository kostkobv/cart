# Cart

[![pipeline status](https://gitlab.com/kostkobv/cart/badges/master/pipeline.svg)](https://gitlab.com/kostkobv/cart/commits/master) [![coverage report](https://gitlab.com/kostkobv/cart/badges/master/coverage.svg)](https://gitlab.com/kostkobv/cart/commits/master)

## How to use the app
Make sure the application was cloned into the correct path in your GOPATH (should be `$GOPATH/src/gitlab.com/kostkobv/cart`) and then just run:
```bash
go run gitlab.com/kostkobv/cart/cmd/...
```

To exit the app, usual `Ctrl+C` should work.

## Inventory
Available products in the inventory

| SKU    | Name           | Price     | Inventory Qty |
|--------|----------------|-----------|---------------|
| 120P90 | Google Home    | $49.99    | 10            |
| 43N23P | MacBook Pro    | $5,399.99 | 5             |
| A304SD | Alexa Speaker  | $109.50   | 10            |
| 234234 | Raspberry Pi B | $30.00    | 2             |

## Discounts
Available discounts:
- Each sale of a MacBook Pro comes with a free Raspberry Pi B
- Buy 3 Google Homes for the price of 2
- Buying more than 3 Alexa Speakers will have a 10% discount on all Alexa speakers

## Implementation FAQ
`Why, according to the source code, discounts are applied every time the cart is fetched?`

Main reason is that in real life discounts could be removed, canceled, expired, changed, etc. By recalculating the prices every time according to the actual state of the discounts, we can eliminate the problem of cart state being unactual or falsy.
