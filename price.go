package cart

import (
	"errors"
	"fmt"
	"math"
	"strconv"
)

// CurrMultiplier defines the multiplier for the Amount
// in the Price struct.
const CurrMultiplier int64 = 10000

// Currency definition code ISO-4217.
type Currency string

// Currency supported values.
const (
	CurrencyAUD Currency = "AUD"
)

var currSymbols = map[Currency]currSymbol{
	CurrencyAUD: {currSym: "$", point: ",", decPoint: "."},
}

type currSymbol struct {
	currSym  string
	symAfter bool
	point    string
	decPoint string
}

var defaultCurrSymbol = currSymbol{currSym: "", point: ",", decPoint: "."}

// Price represents specific amount of money in specific currency.
type Price struct {
	Currency Currency

	// Total price together with cents
	// multiplied by CurrencyMultiplier (10000: ex. $12.34 would make it 123400).
	Amount int64
}

// String to fullfil stringify interface.
// TODO: improve the printout.
func (p *Price) String() string {
	// if we start with price smaller than 1 cent - round it.
	amount := p.Amount
	if minAmount := CurrMultiplier / 100; amount < minAmount {
		amount = int64(math.Round(float64(minAmount)/float64(CurrMultiplier))) * 100
	}

	// get separately amount of cents and full price.
	full := amount / CurrMultiplier

	// round the cents to 2 digits after point.
	// if it makes more than 100 cents - reduce it and add it to full
	// instead.
	cents := math.Round(float64(amount*100)/float64(CurrMultiplier))/100 - float64(full)
	if cents >= 1 {
		full++
		cents--
	}

	// get symbols currency.
	curr, ok := currSymbols[p.Currency]
	if !ok {
		curr = defaultCurrSymbol
	}

	fullStr := strconv.FormatInt(full, 10)

	// mark every thousand with delimiter.
	var result string
	i := len(fullStr)
	for {
		if i-3 <= 0 {
			result = fullStr[:i] + result
			break
		}

		result = curr.point + fullStr[i-3:i] + result
		i -= 3
	}

	// put the result together.
	result = result + curr.decPoint + fmt.Sprintf("%0.2f", cents)[2:]

	// add currency symbol.
	if curr.symAfter {
		return result + curr.currSym
	}

	return curr.currSym + result
}

// Validate the price.
func (p *Price) Validate() error {
	switch {
	case p.Currency == "":
		return fmt.Errorf("invalid currency %s", p.Currency)
	case p.Amount <= 0:
		return errors.New("price cannot be equal or smaller than zero")
	}

	return nil
}
