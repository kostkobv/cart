package cart

import (
	"errors"
)

var (
	// ErrSessionNotExist is returned if session doesn't exist.
	ErrSessionNotExist = errors.New("session doesn't exist")

	// ErrIsEmpty is returned in case if cart is empty.
	ErrIsEmpty = errors.New("cart is empty")

	// ErrUnknownProduct in case of possible operation on unknown product.
	ErrUnknownProduct = errors.New("unknown product")

	// ErrNotEnoughInInventory returned if there is not enough items of the products
	// in the inventory.
	ErrNotEnoughInInventory = errors.New("not enough items of the products in inventory")

	// ErrUnknownCurrency returned in case of unknown currency for the product.
	ErrUnknownCurrency = errors.New("unknown currency")

	// ErrNoDiscounts is returned in case if there are no applicable discounts.
	ErrNoDiscounts = errors.New("no applicable discounts")
)

// LineItem positioned in the Cart.
type LineItem struct {
	Product

	// Amount of items.
	Amount int64 `json:"amount"`
}

// Product that end-user can add to the cart and checkout with it.
type Product struct {
	SKU   string `json:"sku"`
	Name  string `json:"name"`
	Price Price  `json:"-"`
}

// NewProduct is a constructor for a Product.
func NewProduct(sku, name string, p Price) (*Product, error) {
	pr := &Product{SKU: sku, Name: name, Price: p}

	if err := p.Validate(); err != nil {
		return nil, err
	}

	if err := pr.Validate(); err != nil {
		return nil, err
	}

	return pr, nil
}

// Validate the Product.
func (p *Product) Validate() error {
	switch {
	case p.SKU == "":
		return errors.New("sku is required")
	case p.Name == "":
		return errors.New("name is required")
	case p.Price.Amount == 0:
		return errors.New("product doesn't have a price")
	}

	return nil
}
